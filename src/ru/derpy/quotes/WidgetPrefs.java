package ru.derpy.quotes;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent.CanceledException;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.util.DisplayMetrics;

public class WidgetPrefs extends PreferenceActivity {
	
	private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
	private boolean mUpdateWidget = false;
	private boolean mPonySizeChanged = false;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		ResourcesTweaks.tweak(getResources());
		
		super.onCreate(savedInstanceState);
		mAppWidgetId = getWidgetId();
		getPreferenceManager().setSharedPreferencesName(PonyQuotesWidgetProvider.getSharedPreferencesName(mAppWidgetId));
		final Activity activity = this;
		
		addPreferencesFromResource(R.xml.widget_prefs);
		
		findPreference("create_widget").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				mUpdateWidget = true;
				
				Intent resultValue = new Intent();
				resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
				setResult(RESULT_OK, resultValue);
				finish();
				return true;
			}
		});
		
		findPreference("edit_custom_quotes").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				if (!((CheckBoxPreference) preference).isChecked()) {
					((CheckBoxPreference) findPreference("disable_app_quotes")).setChecked(false);
					return true;
				}
				
				File file = PonyQuotesWidgetProvider.getPonyQuotesFile(((ListPreference) findPreference("pony_name")).getValue());
				file.getParentFile().mkdirs();
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				/*Intent intent = new Intent(Intent.ACTION_EDIT, Uri.fromFile(file));
				intent.setData(Uri.fromFile(file));
				intent.setType("text/plain");
				try {
					startActivity(intent);
				} catch (ActivityNotFoundException e)*/ {
					String path = file.getAbsolutePath();
					String cardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
					if (!cardPath.endsWith("/")) cardPath += "/";
					if (path.startsWith(cardPath)) path = path.substring(cardPath.length());
					
					String format = getString(R.string.file_created);
					String msg = String.format(format, path);
					
					AlertDialog.Builder builder = new AlertDialog.Builder(activity);
					builder
						.setMessage(msg)
						.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create(); 
					dialog.setOwnerActivity(activity);
					dialog.show();
				}
				
				return true;
			}
		});
		
		findPreference("show_about").setIntent(new Intent(this, AboutActivity.class));
		
		setupAutoSummary(findPreference("pony_size"));
		setupAutoSummary(findPreference("pony_name"));
		setupAutoSummary(findPreference("font_size"));
		
		{
			final ListPreference pref = (ListPreference) findPreference("pony_size");
			final Preference.OnPreferenceChangeListener oldChangeListener = pref.getOnPreferenceChangeListener();
			pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
				public boolean onPreferenceChange(Preference preference, Object newValue) {
					if (oldChangeListener.onPreferenceChange(preference, newValue)) {
						mPonySizeChanged = true;
						return true;
					}
					return false;
				}
			});
		}
		
		{
			final ListPreference pref = (ListPreference) findPreference("pony_name");
			final Preference.OnPreferenceChangeListener oldChangeListener = pref.getOnPreferenceChangeListener();
			pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
				public boolean onPreferenceChange(Preference preference, Object newValue) {
					if (oldChangeListener.onPreferenceChange(preference, newValue)) {
						if (!mPonySizeChanged) {
							String ponySize; 
							if (newValue.equals("luna") || newValue.equals("big_luna") || newValue.equals("celestia")
								||
								getResources().getDisplayMetrics().densityDpi < DisplayMetrics.DENSITY_MEDIUM) {
								ponySize = "1";
							} else {
								ponySize = "2";
							}
							
							ListPreference ponySizePref = (ListPreference) findPreference("pony_size");
							if (ponySizePref.getOnPreferenceChangeListener().onPreferenceChange(ponySizePref, ponySize)) {
								ponySizePref.setValue(ponySize);
								mPonySizeChanged = false;
							}
						}
						return true;
					}
					return false;
				}
			});
		}
		
		PreferenceScreen ps = getPreferenceScreen();
		if (getResources().getBoolean(R.bool.hide_show_ep)) {
			ps.removePreference(findPreference("show_ep"));
		}
		
		if (!isNewWidget()) {
			ps.removePreference(findPreference("create_widget"));
			mUpdateWidget = true;
		}
	}

	private int getWidgetId() {
		int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    appWidgetId = extras.getInt(
		            AppWidgetManager.EXTRA_APPWIDGET_ID, 
		            AppWidgetManager.INVALID_APPWIDGET_ID);
		    if (appWidgetId != 0) return appWidgetId;
		}
		if (!isNewWidget()) {
			appWidgetId = PonyQuotesWidgetProvider.getWidgetId(getIntent().getData());
		}
		return appWidgetId;
	}
	
	private boolean isNewWidget() {
		Uri uri = getIntent().getData();
		if (uri != null) {
			return !(uri.getScheme().equals("ru.derpy.widget"));
		}
		return true;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		if (mUpdateWidget) {
			try {
				PonyQuotesWidgetProvider.getWidgetUpdateIntent(this, mAppWidgetId).send();
			} catch (CanceledException e) {}
		}
		finish();
	}
	
	private void setupAutoSummary(Preference preference) {
		preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				ListPreference lp = (ListPreference) preference;
				CharSequence summ = lp.getEntries()[lp.findIndexOfValue((String) newValue)];
				preference.setSummary(summ);
				return true;
			}
		});
		preference.setSummary(((ListPreference) preference).getEntry());
	}
}
