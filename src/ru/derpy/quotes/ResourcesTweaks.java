package ru.derpy.quotes;

import java.util.Locale;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class ResourcesTweaks {
	
	public static void tweak(Resources res) {
		Configuration config = new Configuration(res.getConfiguration());
		
		if (config.locale.getLanguage().equals("uk")) config.locale = new Locale("ru");
		
		res.updateConfiguration(config, null);
	}
	
	public static void tweakTabletOrientation(Resources res) {
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration config = new Configuration(res.getConfiguration());
		config.orientation = isTablet(dm) ? Configuration.ORIENTATION_LANDSCAPE : Configuration.ORIENTATION_PORTRAIT;
		res.updateConfiguration(config, null);
	}
	
	public static boolean isTablet(DisplayMetrics dm) {
		final double TABLET_SIZE = 6.0;
		
		int w = dm.widthPixels;
        int h = dm.heightPixels;
        double diag = Math.sqrt(w*w + h*h) / dm.densityDpi;
        
        return diag > TABLET_SIZE;
	}
	
}
