package ru.derpy.quotes;

import android.content.Context;
import android.content.SharedPreferences;

public class WidgetSettings {
	
	public String ponyName, imageName, sleepingImageName; 
	float scale, fontSize;
	boolean mirror, showQuotes, showEPQuotes, disableAppQuotes;

	public WidgetSettings(SharedPreferences prefs) {
		ponyName = prefs.getString("pony_name", "luna");
		imageName = ponyName;
		sleepingImageName = imageName + "_sleep";
		scale = Float.valueOf(prefs.getString("pony_size", "1")).floatValue();
		fontSize = Float.valueOf(prefs.getString("font_size", "10"));
		mirror = prefs.getBoolean("mirror_pony", false);
		showQuotes = prefs.getBoolean("show_quotes", true);
		showEPQuotes = prefs.getBoolean("show_ep", false);
		disableAppQuotes = prefs.getBoolean("disable_app_quotes", false);
	}
	
	public boolean hasSleepingImage(Context context) {
		return context.getResources().getIdentifier(sleepingImageName, "drawable", context.getPackageName()) != 0;
	}
	
}
