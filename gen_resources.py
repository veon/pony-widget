# encoding: utf8
import xml.etree.cElementTree as etree


def transform_file(func, src, dst, **kw):
    with file(src, 'rb') as f:
        data = f.read()
    
    data = func(data, **kw)
    
    with file(dst, 'wb') as f:
        f.write(data)
        
def transform_xml_file(func, src, dst, **kw):
    def xml_func(data, **kw):
        doc = etree.XML(data)
        
        doc = func(doc, **kw)
        c = etree.Comment('This file was generated automatically. Don\'t edit. Edit %s instead.' % src)
        c.tail = '\n    '
        doc.insert(0, c)
        
        return etree.tostring(doc, 'utf-8')
    return transform_file(xml_func, src, dst, **kw)
    
def smartphone_to_tablet(doc):
    for item in doc.findall('.//item'):
        item.text = item.text.replace(u'смартфон', u'планшет').replace(u'smartphone', u'tablet')
    return doc
transform_xml_file(smartphone_to_tablet, 'res/values/rainbow_dash_quotes.xml', 'res/values-land/rainbow_dash_quotes.xml')
transform_xml_file(smartphone_to_tablet, 'res/values-ru/rainbow_dash_quotes.xml', 'res/values-ru-land/rainbow_dash_quotes.xml')

def luna_goes_big(doc):
    for elem in doc.findall('string-array'):
        if elem.attrib['name'] == 'luna_quotes':
            elem.attrib['name'] = 'big_luna_quotes'
    return doc
transform_xml_file(luna_goes_big, 'res/values-ru/luna_quotes.xml', 'res/values-ru/big_luna_quotes.xml')

def widget_info(doc, min_height, min_width = '110'):
    doc.attrib['{http://schemas.android.com/apk/res/android}minHeight'] = str(min_height) + 'dp'
    doc.attrib['{http://schemas.android.com/apk/res/android}minWidth'] = str(min_width) + 'dp'
    return doc

transform_xml_file(widget_info, 'res/xml/widget_info.xml', 'res/xml/widget_info_2x1.xml', min_height = 40)
transform_xml_file(widget_info, 'res/xml/widget_info.xml', 'res/xml/widget_info_2x2.xml', min_height = 110)
transform_xml_file(widget_info, 'res/xml/widget_info.xml', 'res/xml/widget_info_3x2.xml', min_width = 180, min_height = 110)
transform_xml_file(widget_info, 'res/xml/widget_info.xml', 'res/xml/widget_info_4x2.xml', min_width = 250, min_height = 110)

def mirror_widget_layout(data):
    return (data
        .replace('alignParentRight', 'TMP')
        .replace('alignParentLeft', 'alignParentRight')
        .replace('TMP', 'alignParentLeft')
        .replace('marginRight', 'marginLeft')
        .replace('toLeftOf', 'toRightOf')
        .replace('@drawable/quote', '@drawable/quote_mirrored')
    )
transform_file(mirror_widget_layout, 'res/layout/pony_widget.xml', 'res/layout/pony_widget_mirrored.xml')