package ru.derpy.quotes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.widget.RemoteViews;

public class PonyQuotesWidgetProvider extends AppWidgetProvider {
    
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
            int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        
        final int N = appWidgetIds.length;
        for (int i=0; i<N; i++) {
            int appWidgetId = appWidgetIds[i];
            SharedPreferences prefs = getSharedPreferences(context, appWidgetId);
            WidgetSettings settings = new WidgetSettings(prefs);
            
            boolean isSleeping = 
                    isPonySleeping(context, settings, appWidgetId) 
                    && settings.hasSleepingImage(context);
            if (updateByTap && isSleeping) {
                markPonyWaking(context, appWidgetId);
                isSleeping = false;
            }
            
            int layoutId = R.layout.default_widget;
            if (settings.showQuotes) {
            	if (isSleeping)
            		layoutId = R.layout.default_widget;
        		else if (settings.mirror)
	            	layoutId = R.layout.pony_widget_mirrored;
	            else
	            	layoutId = R.layout.pony_widget;
            }
            RemoteViews views = new RemoteViews(context.getPackageName(), layoutId);
            
            if (settings.showQuotes && !isSleeping) {
	            String quote = getQuote(getQuoteResources(context), settings.ponyName, settings);
	            views.setTextViewText(R.id.quoteText, quote);
	            views.setFloat(R.id.quoteText, "setTextSize", settings.fontSize);
            }
            
            String imageName = settings.ponyName;
            if (isSleeping) imageName = settings.sleepingImageName;
            views.setImageViewBitmap(R.id.ponyImage, getPonyImage(context, imageName, settings));
            
            if (isSleeping) {
                views.setOnClickPendingIntent(R.id.ponyImage, getWidgetUpdateIntent(context, appWidgetId));
            } else {
                views.setOnClickPendingIntent(R.id.ponyImage, getWidgetSettingsIntent(context, appWidgetId));
                if (settings.showQuotes)
                    views.setOnClickPendingIntent(R.id.quoteText, getWidgetUpdateIntent(context, appWidgetId));
            }
            
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
    
    public static PendingIntent getWidgetUpdateIntent(Context context, int appWidgetId) {
        Intent intent = new Intent(context, PonyQuotesWidgetProvider.class);
        intent.setAction(PonyQuotesWidgetProvider.ACTION_APPWIDGET_UPDATE);
        intent.setData(getWidgetUri(appWidgetId));
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }
    
    public static PendingIntent getWidgetSettingsIntent(Context context, int appWidgetId) {
        Intent intent = new Intent(context, PopupWidgetPrefs.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.setData(getWidgetUri(appWidgetId));
        return PendingIntent.getActivity(context, 0, intent, 0);
    }
    
    public static final String ACTION_APPWIDGET_UPDATE = "ru.derpy.quotes.WIDGET_UPDATE";
    private static boolean updateByTap = false;
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (PonyQuotesWidgetProvider.ACTION_APPWIDGET_UPDATE.equals(intent.getAction())) {
            int id = getWidgetId(intent.getData());
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[] {id});
            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            updateByTap = true;
        }
        try {
            super.onReceive(context, intent);
        } finally {
            updateByTap = false;
        }
    }
    
    public static int getWidgetId(Uri uri) {
        String path = uri.getPath();
        if (path.startsWith("/")) path = path.substring(1);
        return Integer.valueOf(path);
    }
    
    public static Uri getWidgetUri(int appWidgetId) {
        return Uri.parse("ru.derpy.widget://ids/" + String.valueOf(appWidgetId));
    }
    
    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        final int N = appWidgetIds.length;
        for (int i=0; i<N; i++) {
            int appWidgetId = appWidgetIds[i];
            SharedPreferences prefs = getSharedPreferences(context, appWidgetId);
            prefs.edit().clear().commit();
        }
    }
    
    protected String getQuote(Resources res, String ponyName, WidgetSettings settings) {
        TypedArray quotes = null;
        
        File quotesFile = getPonyQuotesFile(ponyName);
        UserQuotesData userQuotes = PonyQuotesWidgetProvider.loadTextFile(quotesFile);
        if (quotesFile.isFile() && (Math.random() < 0.5 || userQuotes.no_builtins || settings.disableAppQuotes)) {
            String[] customQuotes = userQuotes.quotes;
            if (customQuotes.length > 0) {
                return customQuotes[PonyQuotesWidgetProvider.randomIndex(customQuotes.length)];
            } else if (settings.disableAppQuotes) {
            	return "...";
            }
        }
        
        if (Math.random() < 0.5 || PonyQuotesWidgetProvider.isPonyIgnoresGeneralQuotes(res, ponyName)) {
            int quotesId = res.getIdentifier(ponyName + "_quotes", "array", res.getResourcePackageName(R.layout.pony_widget));
            if (quotesId != 0) {
                try {
                    quotes = res.obtainTypedArray(quotesId);
                } catch (Resources.NotFoundException e) {}
            }
        } 
        
        if (quotes == null || quotes.length() == 0) {
            int quotesId = R.array.general_quotes;
            if (settings.showEPQuotes) 
                quotesId = R.array.general_ep_quotes;
            quotes = res.obtainTypedArray(quotesId);
        }
        
        String quote = quotes.getString(PonyQuotesWidgetProvider.randomIndex(quotes.length()));
        quotes.recycle();
        return quote;
    }
    
    protected Resources getQuoteResources(Context context) {
        Resources res = context.getResources();
        ResourcesTweaks.tweak(res);
        ResourcesTweaks.tweakTabletOrientation(res);
        return res;
    }
    
    protected Bitmap getPonyImage(Context context, String imageName, WidgetSettings settings) {
        Resources res = context.getResources();
        int imgId = res.getIdentifier(imageName, "drawable", context.getPackageName());
        if (imgId == 0)
            imgId = R.drawable.derpy;
        Bitmap img = ((BitmapDrawable) res.getDrawable(imgId)).getBitmap();
        
        float scale = settings.scale;
        boolean filtered = Math.abs(scale - Math.round(scale)) > 0.1f;
        int width = Math.round(img.getWidth()*scale);
        int height = Math.round(img.getHeight()*scale);
        if (settings.mirror) width = -width;
        
        Bitmap scaledImg = Bitmap.createScaledBitmap(img, width, height, filtered);
        
        return scaledImg;
    }
    
    public static File getPonyQuotesFile(String ponyName) {
        File storage = Environment.getExternalStorageDirectory();
        File ponyDir = new File(storage, "PonyQuotes");
        return new File(ponyDir, ponyName + ".txt");
    }
    
    public static boolean isPonyIgnoresGeneralQuotes(Resources res, String ponyName) {
        int id = res.getIdentifier(ponyName + "_ignore_general_quotes", "string", res.getResourcePackageName(R.layout.pony_widget));
        return id != 0;
    }
    
    private static int randomIndex(int length) {
        return Math.round((float) Math.random() * length) % length;
    }
    
    private static class UserQuotesData {
        public String[] quotes = new String[] {};
        public boolean no_builtins = false;
    }
    
    private static UserQuotesData loadTextFile(File file) {
        ArrayList<String> lines = new ArrayList<String>();
        UserQuotesData data = new UserQuotesData();
        
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            LineNumberReader reader = new LineNumberReader(isr);
            
            while (true) {
                String s = reader.readLine();
                if (s == null) break;
                s = s.trim();
                
                if (s.contains("NO_BUILTINS")) data.no_builtins = true;
                if (s.startsWith(";") || s == "") continue; // skip comment lines and empty lines
                
                lines.add(s);
            }
        } catch (FileNotFoundException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
        } finally {
            if (fis != null)
                try {
                    fis.close();
                } catch (IOException e) {}
        }
        
        data.quotes = lines.toArray(new String[lines.size()]);
        return data;
    }
    
    private boolean isPonySleeping(Context context, WidgetSettings settings, int appWidgetId) {
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        
        if (settings.ponyName.contains("luna")) {
            hour = (hour + 12) % 24;
        }
        
        if (hour >= 23 || hour < 8)
            return true;
        
        long hoursSinceWakeup = (System.currentTimeMillis() - getLastWakingTime(context, appWidgetId)) / (3600*1000);
        if (hour < 10 && hoursSinceWakeup < 11 )
            return true;
        
        return false;
    }
    
    private File getPonyWakingFile(Context context, int appWidgetId) {
        final File wakesDir = context.getDir("wakes", Context.MODE_PRIVATE);
        return new File(wakesDir, String.valueOf(appWidgetId));
    }
    
    private long getLastWakingTime(Context context, int appWidgetId) {
        return getPonyWakingFile(context, appWidgetId).lastModified();
    }
    
    private void markPonyWaking(Context context, int appWidgetId) {
        final File wake = getPonyWakingFile(context, appWidgetId);
        try {
            wake.createNewFile();
            wake.setLastModified(System.currentTimeMillis());
        } catch (IOException e) {}
    }
    
    static public String getSharedPreferencesName(int appWidgetId) {
        return String.format("pony_widget_%s", appWidgetId);
    }
    
    static private SharedPreferences getSharedPreferences(Context context, int appWidgetId) {
        return context.getSharedPreferences(getSharedPreferencesName(appWidgetId), 0); 
    }
    
}
