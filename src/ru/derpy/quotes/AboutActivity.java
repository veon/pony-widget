package ru.derpy.quotes;


import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class AboutActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		WebView wv = new WebView(this);
		setContentView(wv);
		setTitle(R.string.about_name);
		
		String path = "about.html";
		
		{
			String language = getResources().getConfiguration().locale.getLanguage();
			String localePath = "about-" + language  + ".html";
			if (isAssetExist(localePath)) path = localePath;
		}
		
		wv.loadUrl("file:///android_asset/" + path);
	}
	
	protected boolean isAssetExist(String path)
	{
		try {
			getAssets().open(path).close();
			return true;
		} catch (IOException e) {
			return false;
		}
		
	}

}
